# 交通灯案列源码

#### 介绍
交通灯案列源码，实现手机靠近nfc模块拉起app，手机连接蓝牙模块，控制设备，oled模块显示设备当前状态。

#### 软件架构
软件架构说明

1、Bluetooth文件夹：实现3633模块蓝牙模块与手机连接，通过uart与3861模块进行通讯。

2、Buzzer文件夹：实现通过pwm驱动蜂鸣器蜂鸣。

3、Key文件夹：实现获取按键长按，短按，连按事件。

4、Led文件夹：实现驱动led亮灭功能。

5、nfc文件夹：实现nfc拉起app应用功能。

6、oled文件夹：实现驱动oled显示字符功能。

7、traffic_light文件夹：交通灯案例程序入口。

8、hilink_3861_traffic文件夹：交通灯案例在总的工程里面作为一个编译组件。


#### 使用说明

1.  git clone https://gitee.com/skh_7_0/khdvk-3861b-blank-project.git  下载一个空白工程。

2. git clone https://gitee.com/skh_7_0/traffic-light-case-source-code.git 下载本案例源程序。

3.  按照案例文档，将交通灯源码文件夹放到克隆下来的工程的对应的位置（Bluetooth，Buzzer，Key，Led，nfc，oled，traffic_light，BUILD.gn放在"//khdvk-3861b-blank-project/ applications / chinasoftinc / wifi-iot / app/"下；hilink_3861_traffic放在"//khdvk-3861b-blank-project// vendor / chinasoftinc/"下；config.gni放在“ khdvk-3861b-blank-project/ build / lite / config / subsystem / applications/”下）。

4.  在工程根目录下面，输入“hb set”设置编译路径，选择“wifiiot_hilink_3861_traffic”。

5.  输入“hb build -f”进行编译。

6.  编译输出的bin文件在根目录的“/out/wifiiot_hilink_3861_traffic/Hi3861_wifiiot_app_allinone.bin”。

7.  使用“HiBurn”工具进行烧录到3861模块上。

8.  带有nfc的安卓手机去靠近nfc模块，拉起app，进行控制设备。

#### 使用效果

应用app：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0324/105132_35144d3c_1777606.png "屏幕截图.png")

演示效果：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0324/104949_429403e4_1777606.png "屏幕截图.png")



#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
